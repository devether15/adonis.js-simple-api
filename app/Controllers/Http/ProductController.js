'use strict'
const Product = use('App/Models/Product')


class ProductController {
  async index ({ response }) {

    const products = await Product.all()

    response.status(200).json({
      message: 'Here are your products',
      data: products
    })
  }

  async store ({ request, response, params: { id } }) {
    const {name, reference, stock, final_price, disc} = request.post()

    // save and get instance back
    const product = await Product.create({name, reference, stock, final_price, disc})

    response.status(201).json({
      message: 'Successfully created a new product',
      data: product
    })

  }

  async show ({ request, response, params: { id } }) {
    
    const product = request.product

    response.status(200).json(product)     
  }

  async update ({ request, response, params: { id } }) {  
   
    const { name, description } = request.post()
    const product = request.product     

    product.name = name
    product.description = description
    
    await product.save()

    response.status(200).json({
      message: 'This product was successfully updated',
      data: product
    })  
  }

  async delete ({ request, response, params: { id } }) {
    const product = request.product    

    await product.delete()

    response.status(200).json({
      message: 'This product was successfully deleted',
      id
    })    
  }
}

module.exports = ProductController
