'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table.timestamps()
      table.string('name')
      table.text('description_short')
      table.text('reference')
      table.text('supplier_reference')      
      table.text('stock_quantity')
      table.text('price_wt')
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
