'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table.timestamps()
      table.string('name') 
      table.text('reference')
      table.text('stock')      
      table.text('final_price')
      table.text('disc')
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
